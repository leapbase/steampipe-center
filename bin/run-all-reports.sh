echo "run all reports..."

bin/run-aws-report.sh aws-compliance
bin/run-aws-report.sh aws-insights
bin/run-aws-report.sh aws-perimeter
bin/run-aws-report.sh aws-tags
bin/run-aws-report.sh aws-thrifty

echo "end of running all reports"

