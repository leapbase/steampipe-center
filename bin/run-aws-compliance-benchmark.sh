MOD_NAME=aws-compliance
BENCHMARK_NAME=${1:-"cisa_cyber_essentials"}
echo "run streampipe mod: $MOD_NAME benchmark: $BENCHMARK_NAME ..."
BENCHMARK_OUTPUT_FILE="../output/$MOD_NAME-$BENCHMARK_NAME.html"
cd "steampipe-mod-$MOD_NAME"
steampipe check benchmark.cis_v150 --output html > $BENCHMARK_OUTPUT_FILE
ls -l $BENCHMARK_OUTPUT_FILE

