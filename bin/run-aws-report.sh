REPORT_NAME=${1:-"aws-thrifty"}
echo "run streampipe mod $REPORT_NAME ..."
REPORT_FILE="../output/$REPORT_NAME.html"
cd "steampipe-mod-$REPORT_NAME"
steampipe check all --output html > $REPORT_FILE
ls -l $REPORT_FILE

