# steampipe-center


## Git setup

```
git config user.name $GITLAB_USER_NAME
git config user.email $GITLAB_USER_EMAIL

git config credential.helper 'cache --timeout=3600'
git config credential.helper store

git config pull.rebase false
git config --list
```


## steampipe mods on aws

```
AWS Compliance Mod https://github.com/turbot/steampipe-mod-aws-compliance
AWS Insights Mod https://github.com/turbot/steampipe-mod-aws-insights
AWS Perimeter Mod https://github.com/turbot/steampipe-mod-aws-perimeter
AWS Tags Mod https://github.com/turbot/steampipe-mod-aws-tags
AWS Thrifty Mod https://github.com/turbot/steampipe-mod-aws-thrifty
```


## submodule refresh

```
git submodule init
git submodule update
```


## run aws reports

```
bin/run-aws-report.sh aws-thrifty
bin/run-aws-report.sh aws-insights
bin/run-aws-report.sh aws-tags
bin/run-aws-report.sh aws-perimeter
```

```
bin/run-all-reports.sh
```


## run aws compliance benchmarks

```
bin/run-aws-compliance-benchmark.sh cis_v150
bin/run-aws-compliance-benchmark.sh foundational_security
bin/run-aws-compliance-benchmark.sh gdpr    
bin/run-aws-compliance-benchmark.sh nist_csf
bin/run-aws-compliance-benchmark.sh pci_v321
bin/run-aws-compliance-benchmark.sh rbi_cyber_security
bin/run-aws-compliance-benchmark.sh soc_2  
```

